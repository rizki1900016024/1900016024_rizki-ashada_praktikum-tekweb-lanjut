import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgotComponent } from './auth/forgot/forgot.component';
import { ImagesComponent } from './admin/images/images.component';

const routes: Routes= [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path:'register',
        component: RegisterComponent
    },
    {
        path:'admin',
        loadChildren:()=>import('./admin/admin.module').then(mod=>mod.AdminModule)
    },
    {
        path: 'public',
        loadChildren:()=>import('./public/public.module').then(mod=>mod.PublicModule)
    },
    {
      path:'forgot',
      component:ForgotComponent
    },
    {
        path:'images',
        component:ImagesComponent
    },
    {
        path:'',
        pathMatch:'full',
        redirectTo:'/login'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }